/*///////////////////////////////////////////////////////////////////////////////
FILENAME   : Copy_Constructor.cpp
AUTHOR     : Brian Denton <brian.denton@gmail.com>
DATE       : 10/17/2012
PROJECT    : 
DESCRIPTION:
///////////////////////////////////////////////////////////////////////////////*/

#include <vector>
#include <iostream>
#include <iomanip>

class X{

public:

  // Default constructor
  X(){
    for( unsigned int i = 0; i < 1E6; ++i )
      vec.push_back( (double)i );

    std::cout << "Default constructor called." << std::endl;
  }

  // Copy constructor
  X( const X& x ){

    vec = x.vec;

    std::cout << "Copy constructor called." << std::endl;
  }

  // Overload assignment operator
  X& operator=( const X& x ){

    vec = x.vec;

    std::cout << "Assignment operator called." << std::endl;

    return *this;

  }



private:

  std::vector< double > vec;

};


int main(){

  X x1 = X();
  X x2 = X();

  x1 = x2;


  std::cout << "\n\n\n";

  std::vector< X > X_vector;


  // The copy constructor is called several times after each call to 
  // the default constructor because the addition of a new element to the
  // vector requires a reallocation to a new memory block.
  for( unsigned int j = 0; j < 5; ++j ){
    
    std::cout << "\nj = " << j << "\n";

    X_vector.push_back( X() );
    std::cout << "vector capacity: " << X_vector.capacity() << "\t\t"
	      << "vector size: " << X_vector.size() << std::endl; 
  }

  return 0;
}

