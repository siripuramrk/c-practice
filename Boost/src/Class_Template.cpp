/*///////////////////////////////////////////////////////////////////////////////
FILENAME   : Class_Template.cpp
AUTHOR     : Brian Denton <brian.denton@gmail.com>
DATE       : 01/22/2013
PROJECT    : 
DESCRIPTION:
///////////////////////////////////////////////////////////////////////////////*/

#include <string>
#include <vector>
#include <iostream>
#include <iomanip>
//#include "/variant/variant.hpp"
#include "/home/c065288/usr/boost_1_52_0/boost/variant/variant.hpp"

template <class T>
class Container{

public:

  // Default constructor
  Container( T x_, double index_ ){
    x = x_;
    index = index_;
  }


  void print(){
    std::cout << "Index:" << std::setw(3) << index << std::setw(5)
	      << "x: " << x << std::endl;

  }


private:

  T x;
  double index;

};


int main(){

  Container<int> int1 = Container<int>( 1, 1 );
  Container<int> int2 = Container<int>( 2, 10 );
  Container<int> int3 = Container<int>( 3, 20 );
  Container<std::string> str1 = Container<std::string>( "String A", 15 );
  Container<std::string> str2 = Container<std::string>( "String B", 8 );
  Container<std::string> str3 = Container<std::string>( "String C", 2 );

  int1.print();
  str1.print();

  //std::vector< Container > vec;

  //vec.push_back( int1 );
  //vec.push_back( str1 );


  return 0;
}

// END OF FILE
