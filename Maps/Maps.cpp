/*///////////////////////////////////////////////////////////////////////////////
FILENAME   : Maps.cpp
AUTHOR     : Brian Denton <brian.denton@gmail.com>
DATE       : 03/07/2013
PROJECT    : 
DESCRIPTION:
///////////////////////////////////////////////////////////////////////////////*/

#include <iostream>
#include <map>
#include <string>

using namespace std;

int main(){

  int x = 2;
  int previous = 1;

  // map< int, string > map1;

  // map1[1] = "one";
  // map1[2] = "two";
  // map1[2] = "two again";


  // cout << "map1[1]: " << map1.at(1) << endl;

  // cout << "map1[2]: " << map1.at(2) << endl;

  while( previous < x ){
    x = 2*x;
    previous = 2*previous;

  }

  cout << "x: " << x << endl
       << "previous: " << previous << endl;

  cout << endl;

  return 0;

}

//END OF FILE
