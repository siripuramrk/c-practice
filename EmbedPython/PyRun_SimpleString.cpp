/*///////////////////////////////////////////////////////////////////////////////
FILENAME   : PyRun_SimpleString.cpp
AUTHOR     : Brian Denton <brian.denton@gmail.com>
DATE       : 11/07/2012
DESCRIPTION:
///////////////////////////////////////////////////////////////////////////////*/

#include <Python.h>

int main(){

  Py_Initialize(); //boilerplate
  
  PyRun_SimpleString( "name = raw_input('Name: ' )\n"
  		      "print 'Hi,', name\n" );
  
  Py_Finalize(); //boilerplate

  return 0;
}


// END OF FILE
