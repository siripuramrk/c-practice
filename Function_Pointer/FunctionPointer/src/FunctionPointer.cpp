/*///////////////////////////////////////////////////////////////////////////////
FILENAME   : FunctionPointer.cpp
AUTHOR     : Brian Denton <brian.denton@gmail.com>
DATE       : 10/23/2012
PROJECT    : 
DESCRIPTION:
///////////////////////////////////////////////////////////////////////////////*/

#include <iostream>
#include <Rcpp.h>

class FunctionPointer{

private:

  double x;

public:

  FunctionPointer( double x_ ){
    x = x_;
  }

  void add( double y ){
    x += y;
  }


  double getX(){
    return x;
  }




};

RCPP_MODULE( mod_function_pointer ){

  using namespace Rcpp;
  
  class_<FunctionPointer>( "FunctionPointer" )
    
    .constructor< double >()

    // Assign C++ methods to their aliases in R.
    // Also provide (optional) docstring.

    .method( "add", &FunctionPointer::add )
    .method( "x", &FunctionPointer::getX )
   ;
}


// END OF FILE
