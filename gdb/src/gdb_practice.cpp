/*///////////////////////////////////////////////////////////////////////////////
FILENAME   : gdb_practice.cpp
AUTHOR     : Brian Denton <brian.denton@gmail.com>
DATE       : 10/17/2012
PROJECT    : 
DESCRIPTION:
///////////////////////////////////////////////////////////////////////////////*/

// The architecture of the compiled executable and gdb must be the same: if gdb
// is compiled as 32-bit (e.g. i686-pc-linux-gnu) then the executable must be
// compiled with the -m32 flag passed to gcc.

// Once the core dump or executable is loaded into gdb you can call: 
// run, where, backtrace, list, and print to navigate through the program to
// track down the source of the error.


#include <iostream>

int main(){

  int* p = NULL;


  std::cout << "Dereferening a null pointer: ";

  *p = 10;

  return 0;
}

//END OF FILE

